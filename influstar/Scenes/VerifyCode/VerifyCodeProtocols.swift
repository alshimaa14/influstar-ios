//
//  VerifyCodeProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol VerifyCodeRouterProtocol: BaseRouterProtocol {
    func navigateToNewPassword(code: String)
    func dismiss()
}

protocol VerifyCodePresenterProtocol: class {
    var view: VerifyCodeViewProtocol? { get set }
    func verifyCode(firstDigit: String, secondDigit: String, thirdDigit: String, forthDigit: String)
    func dismissViewController()
}

protocol VerifyCodeInteractorInputProtocol: class {
    var presenter: VerifyCodeInteractorOutputProtocol? { get set }
    func verifyCode(parameters: [String: Any], code: String)
}

protocol VerifyCodeInteractorOutputProtocol: class {
    func didVerifyCode(with result: Result<Data>, code: String)
}

protocol VerifyCodeViewProtocol: HasActivityIndicator {
    var presenter: VerifyCodePresenterProtocol! { get set }
}
