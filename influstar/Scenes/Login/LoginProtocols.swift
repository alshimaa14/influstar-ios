//
//  LoginProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol LoginRouterProtocol: BaseRouterProtocol {
    func navigate(to destination: LoginDestination)
    func dismiss()
}

protocol LoginPresenterProtocol: class {
    var view: LoginViewProtocol? { get set }
    func login(email: String, password: String)
    func navigateToAddProfile()
    func navigateToForgetPassword()
    func dismissViewController()
}

protocol LoginInteractorInputProtocol: class {
    var presenter: LoginInteractorOutputProtocol? { get set }
    func login(parameters: [String: Any])
}

protocol LoginInteractorOutputProtocol: class {
    func didLogin(with result: Result<User>)
}

protocol LoginViewProtocol: HasActivityIndicator {
    var presenter: LoginPresenterProtocol! { get set }
}
