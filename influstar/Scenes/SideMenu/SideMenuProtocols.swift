//
//  SideMenuProtocols.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol SideMenuRouterProtocol: BaseRouterProtocol {
    func navigateToItems(to destination: SideMenuDestination)
}

protocol SideMenuPresenterProtocol: class {
    var view: SideMenuViewProtocol? { get set }
    func viewDidLoad()
    func didSelectRow(at indexPath: IndexPath)
}

protocol SideMenuInteractorInputProtocol: class {
    var presenter: SideMenuInteractorOutputProtocol? { get set }
}

protocol SideMenuInteractorOutputProtocol: class {
    
}

protocol SideMenuViewProtocol: class {
    var presenter: SideMenuPresenterProtocol! { get set }
    func reloadTableView()
}
