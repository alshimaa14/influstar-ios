//
//  BaseCollectionViewController.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BaseCollectionViewView: UIView {
    
    let refreshControl = UIRefreshControl()
    
    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .clear
        collectionView.refreshControl = refreshControl
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BaseCollectionViewController<M, P: BaseListPresenterProtocol, C: BaseCollectionViewCell<M>>: BaseViewController, BaseListViewProtocol, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout where P.Model == M {
    
    var baseMainView: BaseCollectionViewView!
    var basePresenter: P!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseMainView.collectionView.dataSource = self
        baseMainView.collectionView.delegate = self
        baseMainView.collectionView.register(C.self, forCellWithReuseIdentifier: C.className)
        baseMainView.refreshControl.addAction(for: .valueChanged) { [weak self] in
            guard let self = self else { return }
            self.basePresenter.pullToRefresh()
        }
    }
    
    func reloadData() {
        baseMainView.collectionView.reloadData()
    }
    
    func endRefreshing() {
        UIView.animate(withDuration: 0.2, animations: {
            self.baseMainView.refreshControl.endRefreshing()
            self.baseMainView.collectionView.contentOffset = .zero
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return basePresenter.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: C = collectionView.dequeueCell(for: indexPath)
        basePresenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        fatalError("Must override sizeForItemAt")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        basePresenter.didSelectRow(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == collectionView.numberOfItems(inSection: 0) - 1 {
            basePresenter.loadNextPage()
        }
    }
    
    func setEmptyText(text: String) {
        baseMainView.collectionView.emptyMessage(message: text)
    }
}
