//
//  NewPasswordView.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "bg"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "white_back"), for: .normal)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 18)
        label.text = "haveAProfile".localized
        label.textColor = .white
        return label
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        return UIView()
    }()
    
    private lazy var inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 17
        return view
    }()
    
    private lazy var enterEmailLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 15)
        label.textColor = .white
        label.text = "pleaseEnterNewPassword".localized
        return label
    }()
    
    lazy var passwordTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "password".localized
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var confirmPasswordTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "password".localized
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var sendButton: Button = {
        let button = Button.create()
        button.setTitle("done".localized, for: .normal)
        return button
    }()
    
    lazy var sittingGirlImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "sitting_girl"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(backButton)
        addSubview(titleLabel)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(enterEmailLabel)
        containerView.addSubview(inputsContainerView)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(confirmPasswordTextField)
        inputsContainerView.addSubview(sendButton)
        inputsContainerView.addSubview(sittingGirlImageView)
    }
    
    private func setupBackgroundImageViewConstarints() {
        backgroundImageView.pinAllEdges()
    }
    
    private func setupBackButtonConstrainsts() {
        backButton.constraint {
            $0.top(UIDevice.hasTopNotch ? 55 : 50)
            $0.leading(18)
            $0.width(20)
            $0.height(20)
        }
    }
    
    private func setupTitleLabelConstraints() {
        titleLabel.constraint {
            $0.centerX(0)
            $0.centerY(0, toView: backButton)
        }
    }
    
    private func setupScrollViewConstraints() {
        scrollView.constraint {
            $0.topToBottom(ofView: backButton, withPadding: 0)
            $0.bottom(0)
            $0.leading(0)
            $0.trailing(0)
        }
    }
    
    private func setupContainerViewConstraints() {
        containerView.pinAllEdges()
        containerView.constraint {
            $0.width(multiplier: 1.0)
        }
    }
    
    private func setupEnterEmailLabelConstraints() {
        enterEmailLabel.constraint {
            $0.top(48)
            $0.leading(16, toView: inputsContainerView)
        }
    }
    
    private func setupInputsContainerViewConstraints() {
        inputsContainerView.constraint {
            $0.topToBottom(ofView: enterEmailLabel, withPadding: 12)
            $0.leading(16)
            $0.trailing(16)
            $0.bottom(16)
        }
    }
    
    private func setupPasswordTextFieldConstraints() {
        passwordTextField.constraint {
            $0.top(48)
            $0.leading(28)
            $0.trailing(28)
        }
    }
    
    private func setupConfirmPasswordTextFieldConstraints() {
        confirmPasswordTextField.constraint {
            $0.topToBottom(ofView: passwordTextField, withPadding: 13.5)
            $0.leading(28)
            $0.trailing(28)
        }
    }
    
    private func setupSendButtonConstraints() {
        sendButton.constraint {
            $0.topToBottom(ofView: confirmPasswordTextField, withPadding: 35)
            $0.leading(26)
            $0.trailing(26)
        }
    }
    
    
    private func setupSittingGirlImageViewConstraints() {
        sittingGirlImageView.constraint {
            $0.topToBottom(ofView: sendButton, withPadding: 18)
            $0.trailing(-2)
            $0.bottom(-8)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstarints()
        setupBackButtonConstrainsts()
        setupTitleLabelConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupEnterEmailLabelConstraints()
        setupInputsContainerViewConstraints()
        setupPasswordTextFieldConstraints()
        setupConfirmPasswordTextFieldConstraints()
        setupContainerViewConstraints()
        setupSendButtonConstraints()
        setupSittingGirlImageViewConstraints()
    }
    
}
