//
//  AddProfileView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AddProfileView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "bg"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "white_back"), for: .normal)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 18)
        label.text = "haveAProfile".localized
        label.textColor = .white
        return label
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        return UIView()
    }()
    
    private lazy var headImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "head"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 17
        return view
    }()
    
    private lazy var feetImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "feet"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var registerNowLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 15)
        label.textColor = .white
        label.text = "helloRegisterNow".localized
        return label
    }()
    
    lazy var usernameTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "username".localized
        return textField
    }()

    lazy var emailTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "email".localized
        return textField
    }()
    
    lazy var passwordTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "password".localized
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var confirmPasswordTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "confirmPassword".localized
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var termsAndConditionsButton: CheckBoxButton = {
        let checkBoxButton = CheckBoxButton.create(uncheckedImage: UIImage(named: "ic_unchecked"), checkedImage: UIImage(named: "ic_checked"))
        let attributedString = NSMutableAttributedString(string: "iAgreeWith".localized, attributes: [
            .foregroundColor: UIColor.black,
            .font: AvenirNextFont.regular.getFont(ofSize: 13)
        ])
        attributedString.append(NSAttributedString(string: " " + "termsAndConditions".localized, attributes: [
            .foregroundColor: UIColor.appColor,
            .font: AvenirNextFont.regular.getFont(ofSize: 13)
        ]))
        checkBoxButton.setAttributedTitle(attributedString, for: .normal)
        return checkBoxButton
    }()
    
    lazy var registerButton: Button = {
        let button = Button.create()
        button.setTitle("register".localized, for: .normal)
        return button
    }()
    
    lazy var profileButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = AvenirNextFont.demi.getFont(ofSize: 13)
        button.setTitleColor(.appColor, for: .normal)
        button.setTitle("alreadyHaveAProfile".localized, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(backButton)
        addSubview(titleLabel)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(inputsContainerView)
        containerView.addSubview(headImageView)
        containerView.addSubview(feetImageView)
        containerView.addSubview(registerNowLabel)
        inputsContainerView.addSubview(usernameTextField)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(confirmPasswordTextField)
        inputsContainerView.addSubview(termsAndConditionsButton)
        inputsContainerView.addSubview(registerButton)
        inputsContainerView.addSubview(profileButton)
    }
    
    private func setupBackgroundImageViewConstarints() {
        backgroundImageView.pinAllEdges()
    }
    
    private func setupBackButtonConstrainsts() {
        backButton.constraint {
            $0.top(UIDevice.hasTopNotch ? 55 : 50)
            $0.leading(18)
            $0.width(40)
            $0.height(40)
        }
    }
    
    private func setupTitleLabelConstraints() {
        titleLabel.constraint {
            $0.centerX(0)
            $0.centerY(0, toView: backButton)
        }
    }
    
    private func setupScrollViewConstraints() {
        scrollView.constraint {
            $0.topToBottom(ofView: backButton, withPadding: 0)
            $0.bottom(0)
            $0.leading(0)
            $0.trailing(0)
        }
    }
    
    private func setupContainerViewConstraints() {
        containerView.pinAllEdges()
        containerView.constraint {
            $0.width(multiplier: 1.0)
        }
    }
    
    private func setupHeadImageViewConstraints() {
        headImageView.constraint {
            $0.top(0)
            $0.trailing(0, toView: inputsContainerView)
        }
    }
    
    private func setupInputsContainerViewConstraints() {
        inputsContainerView.constraint {
            $0.topToBottom(ofView: headImageView, withPadding: 0)
            $0.leading(16)
            $0.trailing(16)
            $0.bottom(16)
        }
    }
    
    private func setupFeetImageViewConstraints() {
        feetImageView.constraint {
            $0.topToBottom(ofView: headImageView, withPadding: 0)
            $0.trailing(10, toView: headImageView)
        }
    }
    
    private func setupRegisterNowLabelConstraints() {
        registerNowLabel.constraint {
            $0.leading(16, toView: inputsContainerView)
            $0.bottomToTop(ofView: inputsContainerView, withPadding: 8)
        }
    }
    
    private func setupUsernameTextFieldConstraints() {
        usernameTextField.constraint {
            $0.topToBottom(ofView: feetImageView, withPadding: 8)
            $0.leading(25)
            $0.trailing(25)
        }
    }
    
    private func setupEmailTextFieldConstraints() {
        emailTextField.constraint {
            $0.topToBottom(ofView: usernameTextField, withPadding: 13.5)
            $0.leading(25)
            $0.trailing(25)
        }
    }
    
    private func setupPasswordTextFieldConstraints() {
        passwordTextField.constraint {
            $0.topToBottom(ofView: emailTextField, withPadding: 13.5)
            $0.leading(25)
            $0.trailing(25)
        }
    }
    
    private func setupConfirmPasswordTextFieldConstraints() {
        confirmPasswordTextField.constraint {
            $0.topToBottom(ofView: passwordTextField, withPadding: 13.5)
            $0.leading(25)
            $0.trailing(25)
        }
    }
  
    private func setupTermsAndConditionsButtonConstraints() {
        termsAndConditionsButton.constraint {
            $0.topToBottom(ofView: confirmPasswordTextField, withPadding: 20)
            $0.centerX(0)
        }
    }
    
    private func setupRegisterButtonConstraints() {
        registerButton.constraint {
            $0.topToBottom(ofView: termsAndConditionsButton, withPadding: 25)
            $0.leading(26)
            $0.trailing(26)
        }
    }
    
    private func setupProfileButtonConstraints() {
        profileButton.constraint {
            $0.topToBottom(ofView: registerButton, withPadding: 18)
            $0.centerX(0)
            $0.bottom(27)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstarints()
        setupBackButtonConstrainsts()
        setupTitleLabelConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupHeadImageViewConstraints()
        setupInputsContainerViewConstraints()
        setupFeetImageViewConstraints()
        setupRegisterNowLabelConstraints()
        setupUsernameTextFieldConstraints()
        setupEmailTextFieldConstraints()
        setupPasswordTextFieldConstraints()
        setupConfirmPasswordTextFieldConstraints()
        setupTermsAndConditionsButtonConstraints()
        setupRegisterButtonConstraints()
        setupProfileButtonConstraints()
    }
    
}
