//
//  MyProfilePresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class MyProfilePresenter: BaseListPresenter<Social>, MyProfilePresenterProtocol, MyProfileInteractorOutputProtocol {
    
    weak var view: MyProfileViewProtocol?
    private let interactor: MyProfileInteractorInputProtocol
    private let router: MyProfileRouterProtocol
    
    init(view: MyProfileViewProtocol, interactor: MyProfileInteractorInputProtocol, router: MyProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        super.init()
        baseView = view as? BaseListViewProtocol
    }
    
    func viewDidLoad() {
        items.append(contentsOf: [Social(image: "ic_twitter", color: .twitterThemeColor),
                                  Social(image: "ic_facebook", color: .facebookThemeColor),
                                  Social(image: "ic_linkedin", color: .linkedinThemeColor),
                                  Social(image: "ic_instagram", color: .instagramThemeColor)])
    }
    
    func addMoreButtonTapped() {
        items.append(contentsOf: [Social(image: "ic_twitter", color: .twitterThemeColor),
                                  Social(image: "ic_facebook", color: .facebookThemeColor),
                                  Social(image: "ic_linkedin", color: .linkedinThemeColor),
                                  Social(image: "ic_instagram", color: .instagramThemeColor)])
    }
    
    func openMenu() {
        router.openSideMenu()
    }
    
    func plusButtonTapped() {
    }
    
}
