//
//  SideMenuRouter.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum SideMenuDestination {
    case about
    case login
}

class SideMenuRouter: BaseRouter, SideMenuRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = SideMenuViewController()
        let interactor = SideMenuInteractor()
        let router = SideMenuRouter()
        let presenter = SideMenuPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToItems(to destination: SideMenuDestination) {
        switch destination {
        case .about:
            self.viewController?.show(AboutRouter.createModule(), sender: self)
        case .login:
            self.viewController?.show(LoginRouter.createModule(), sender: self)
        }
    }
    
}
