//
//  TextView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TextView: UIView {
    
    var text: String {
        return textView.text
    }
    
    private let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    private let textView: UITextView = {
        let textView = UITextView()
        textView.textContainerInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        textView.backgroundColor = .clear
        textView.textContainer.lineFragmentPadding = 0
        return textView
    }()
    
    private let height: CGFloat
    
    init(height: CGFloat) {
        self.height = height
        super.init(frame: .zero)
        constraint {
            $0.height(height + 8)
        }
        layoutUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        addSubview(textView)
        addSubview(lineView)
    }

    private func setupTextViewConstraints() {
        textView.constraint {
            $0.leading(0)
            $0.trailing(0)
            $0.height(height)
            $0.centerX(0)
        }
    }
    
    private func setupLineViewConstraints() {
        lineView.constraint {
            $0.leading(0)
            $0.trailing(0)
            $0.centerX(0)
            $0.topToBottom(ofView: textView, withPadding: 8)
            $0.height(1)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupTextViewConstraints()
        setupLineViewConstraints()
    }
    
}
