//
//  AddProfileProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol AddProfileRouterProtocol: BaseRouterProtocol {
    func dismiss()
    func navigate(to destination: RegisterDestination)
}

protocol AddProfilePresenterProtocol: class {
    var view: AddProfileViewProtocol? { get set }
    func register(userName: String, email: String, password: String, confirmPassword: String, isAcceptedConditions: Bool)
    func dismissViewController()
    func navigateToLogin()
}

protocol AddProfileInteractorInputProtocol: class {
    var presenter: AddProfileInteractorOutputProtocol? { get set }
    func addProfile(parameters: [String: Any])
}

protocol AddProfileInteractorOutputProtocol: class {
    func didFetchAddProfile(with result: Result<User>)
}

protocol AddProfileViewProtocol: HasActivityIndicator {
    var presenter: AddProfilePresenterProtocol! { get set }
}
