//
//  AboutProtocols.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol AboutRouterProtocol: BaseRouterProtocol {
    
}

protocol AboutPresenterProtocol: class {
    var view: AboutViewProtocol? { get set }
}

protocol AboutInteractorInputProtocol: class {
    var presenter: AboutInteractorOutputProtocol? { get set }
}

protocol AboutInteractorOutputProtocol: class {
    
}

protocol AboutViewProtocol: class {
    var presenter: AboutPresenterProtocol! { get set }
}
