//
//  UICollectionViewExtension.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UICollectionView {
    convenience init<T: UICollectionViewCell>(cellType: T.Type, layout: UICollectionViewFlowLayout) {
        self.init(frame: .zero, collectionViewLayout: layout)
        register(cellType.self, forCellWithReuseIdentifier: cellType.className)
    }
    
    func dequeueCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as! T
    }
    
    func emptyMessage(message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 5))
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
//        messageLabel.font = MontserratFont.bold.getFont(ofSize: 17)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        backgroundView = view
        backgroundView!.addSubview(messageLabel)
        messageLabel.constraint {
            $0.centerX(0)
            $0.centerY(0)
            $0.width(frame.width - 16)
        }
    }
}
