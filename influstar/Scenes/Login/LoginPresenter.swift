//
//  LoginPresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class LoginPresenter: LoginPresenterProtocol, LoginInteractorOutputProtocol {
    
    weak var view: LoginViewProtocol?
    private let interactor: LoginInteractorInputProtocol
    private let router: LoginRouterProtocol
    
    private var user: User?
    
    init(view: LoginViewProtocol, interactor: LoginInteractorInputProtocol, router: LoginRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func login(email: String, password: String) {
        guard !email.isEmpty else {
            router.showAlert(with: "emptyEmail".localized)
            return
        }
        guard !password.isEmpty else {
            router.showAlert(with: "emptyPassword".localized)
            return
        }
        
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        let parameter: [String: Any] =  ["email": email,
                                         "password": password]
        interactor.login(parameters: parameter)
    }
    
    func didLogin(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let result):
            user = result
            UserDefaultsHelper.isLoggedIn = true
            router.navigate(to: .profile)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func navigateToAddProfile() {
        router.navigate(to: .addProfile)
    }
    
    func navigateToForgetPassword() {
        router.navigate(to: .forgetPassword)
    }
    
    func dismissViewController() {
        router.dismiss()
    }
}
