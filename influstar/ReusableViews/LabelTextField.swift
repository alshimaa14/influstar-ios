//
//  LabelTextFIeld.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LabelTextField: UIView {
    
    var text: String {
        return textField.text!
    }
    
    var isSecureTextEntry: Bool = false {
        didSet {
            textField.isSecureTextEntry = isSecureTextEntry
        }
    }
    
    lazy var topLabel: UILabel = {
        let label = UILabel()
        label.textColor = .appColor
        label.font = AvenirNextFont.demi.getFont(ofSize: 14)
        return label
    }()
    
    private lazy var textField: TextField = {
        return TextField()
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        constraint {
            $0.height(68)
        }
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(topLabel)
        addSubview(textField)
    }
    
    private func setupTopLabelConstraints() {
        topLabel.constraint {
            $0.top(0)
            $0.leading(0)
            $0.trailing(0)
        }
    }
    
    private func setupTextFieldConstraints() {
        textField.constraint {
            $0.topToBottom(ofView: topLabel, withPadding: 0)
            $0.leading(0)
            $0.trailing(0)
            $0.height(34)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupTopLabelConstraints()
        setupTextFieldConstraints()
    }
}
