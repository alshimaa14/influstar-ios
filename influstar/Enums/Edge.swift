//
//  Edge.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum Edge {
    case safeAreaTop(CGFloat)
    case top(CGFloat)
    case leading(CGFloat)
    case trailing(CGFloat)
    case bottom(CGFloat)
    case safeAreaBottom(CGFloat)
}
