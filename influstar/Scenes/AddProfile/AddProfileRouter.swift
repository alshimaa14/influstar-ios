//
//  AddProfileRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum RegisterDestination {
    case login
    case termsAndConditions
    case profile
}

class AddProfileRouter: BaseRouter, AddProfileRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = AddProfileViewController()
        let interactor = AddProfileInteractor()
        let router = AddProfileRouter()
        let presenter = AddProfilePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func dismiss() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func navigate(to destination: RegisterDestination) {
        switch destination {
        case .termsAndConditions:
            break
        case .login:
            self.viewController?.navigationController?.pushViewController(LoginRouter.createModule(), animated: true)
        case .profile:
            AppDelegate.shared.setRootViewController(
                MyProfileRouter.createModule(),
                animated: false
            )
        }
    }
    
}
