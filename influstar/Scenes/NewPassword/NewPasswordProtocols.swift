//
//  NewPasswordProtocols.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol NewPasswordRouterProtocol: BaseRouterProtocol {
    func navigateToLogin()
    func dismiss()
}

protocol NewPasswordPresenterProtocol: class {
    var view: NewPasswordViewProtocol? { get set }
    func newPassword(password: String, confirmPaswword: String)
    func dismissViewController()
}

protocol NewPasswordInteractorInputProtocol: class {
    var presenter: NewPasswordInteractorOutputProtocol? { get set }
    func newPassword(parameters: [String: Any])
}

protocol NewPasswordInteractorOutputProtocol: class {
    func didAddNewPassword(with result: Result<Data>)
}

protocol NewPasswordViewProtocol: HasActivityIndicator {
    var presenter: NewPasswordPresenterProtocol! { get set }
}
