//
//  ChooseLanguageProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

enum Language: String {
    case english = "en", arabic = "ar", french = "fr", german = "gr"
}

protocol ChooseLanguageRouterProtocol: BaseRouterProtocol {
    func navigateToProfile(language: Language)
}

protocol ChooseLanguagePresenterProtocol: class {
    var view: ChooseLanguageViewProtocol? { get set }
    func navigateToProfile(language: Language)
}

protocol ChooseLanguageInteractorInputProtocol: class {
    var presenter: ChooseLanguageInteractorOutputProtocol? { get set }
}

protocol ChooseLanguageInteractorOutputProtocol: class {
    
}

protocol ChooseLanguageViewProtocol: class {
    var presenter: ChooseLanguagePresenterProtocol! { get set }
}
