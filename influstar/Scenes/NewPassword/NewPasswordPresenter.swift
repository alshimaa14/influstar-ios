//
//  NewPasswordPresenter.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class NewPasswordPresenter: NewPasswordPresenterProtocol, NewPasswordInteractorOutputProtocol {
    
    weak var view: NewPasswordViewProtocol?
    private let interactor: NewPasswordInteractorInputProtocol
    private let router: NewPasswordRouterProtocol
    
    private var code: String
    
    init(view: NewPasswordViewProtocol, interactor: NewPasswordInteractorInputProtocol, router: NewPasswordRouterProtocol, code: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.code = code
    }
    
    func newPassword(password: String, confirmPaswword: String) {
        guard !password.isEmpty else {
            router.showAlert(with: "emptyPassword".localized)
            return
        }
        
        guard !confirmPaswword.isEmpty else {
            router.showAlert(with: "emptyConfirmPassword".localized)
            return
        }
        
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        let parameter: [String: Any] =  ["code": code,
                                         "new_password": password,
                                         "confirm_password": confirmPaswword]
        interactor.newPassword(parameters: parameter)
    }
    
    func didAddNewPassword(with result: Result<Data>) {
        switch result {
        case .success:
            router.navigateToLogin()
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func dismissViewController() {
        router.dismiss()
    }
    
}
