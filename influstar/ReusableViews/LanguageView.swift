//
//  LanguageView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LanguageView: UIView {
    
    lazy var flagImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "egypt"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var languageLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 14)
        label.textColor = .rgba(54, 49, 116, 1)
        label.text = "Arabic"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 19
        constraint {
            $0.height(108)
            $0.width(132)
        }
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func addSubviews() {
        addSubview(flagImageView)
        addSubview(languageLabel)
    }
    
    private func setupFlagImageViewConstraints() {
        flagImageView.constraint {
            $0.top(26)
            $0.height(30)
            $0.width(45)
            $0.centerX(0)
        }
    }
    
    private func setupLanguageLabelConstraints() {
        languageLabel.constraint {
            $0.topToBottom(ofView: flagImageView, withPadding: 12)
            $0.centerX(0)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupFlagImageViewConstraints()
        setupLanguageLabelConstraints()
    }
}
