//
//  CheckBoxButton.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CheckBoxButton: UIButton {
    
    private var uncheckedImage: UIImage?
    private var checkedImage: UIImage?
    var isChecked = false
    
    static func create(uncheckedImage: UIImage?, checkedImage: UIImage?) -> CheckBoxButton {
        let checkBoxButton = CheckBoxButton(type: .system)
        checkBoxButton.uncheckedImage = uncheckedImage?.withRenderingMode(.alwaysOriginal)
        checkBoxButton.checkedImage = checkedImage?.withRenderingMode(.alwaysOriginal)
        checkBoxButton.initialize()
        return checkBoxButton
    }
    
    private func initialize() {
        setImage(uncheckedImage, for: .normal)
        imageView!.constraint {
            $0.centerY(0)
            $0.leading(-10)
        }
        handleOnTap()
    }
    
    private func handleOnTap() {
        onTap {
            self.setImage(self.isChecked ? self.uncheckedImage : self.checkedImage, for: .normal)
            self.isChecked.toggle()
        }
    }
    
}
