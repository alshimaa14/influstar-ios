//
//  UIImageViewExtension.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    
    func load(url: URL?, placeholder: UIImage? = nil) {
        if url?.absoluteString.hasPrefix("file://") == true {
            guard let url = url else { return }
            let provider = LocalFileImageDataProvider(fileURL: url)
            kf.setImage(with: provider, placeholder: placeholder)
        } else {
            var kf = self.kf
            kf.indicatorType = .activity
            kf.setImage(with: url, placeholder: placeholder)
        }
    }
    
}
