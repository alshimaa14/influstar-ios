//
//  CustomNavigationController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/30/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.black
        ]
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = false
        navigationBar.tintColor = .black
        navigationBar.layer.masksToBounds = false
        navigationBar.layer.applySketchShadow(color: .rgba(193, 193, 193, 0.16), alpha: 1.0, x: 0, y: 3, blur: 10, spread: 0)
    }
    
}
