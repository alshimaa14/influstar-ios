//
//  SideMenuView.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SideMenuView: BaseTableViewView {
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "logo_colored")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        tableView.separatorStyle = .none
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(logoImageView)
        addSubview(tableView)
    }
    
    private func setupLogoImageViewConstraints() {
        logoImageView.constraint {
            $0.top(0)
            $0.centerX(0)
//            $0.leading(0)
//            $0.trailing(0)
//            $0.height(100)
        }
    }
    
    private func setupTableViewConstraints() {
        tableView.constraint {
            $0.topToBottom(ofView: logoImageView, withPadding: 0)
            $0.leading(0)
            $0.trailing(0)
            $0.bottom(0)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupLogoImageViewConstraints()
        setupTableViewConstraints()
    }
    
}
