//
//  AuthenticationRouter.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation
import Alamofire

enum AuthenticationRouter: URLRequestConvertible {
    case addProfile(parameters: [String: Any])
    case login(parameters: [String: Any])
    case forgetPassword(parameters: [String: Any])
    case verificationCode(parameters: [String: Any])
    case newPassword(parameters: [String: Any])
    
    var method: HTTPMethod {
        return .post
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .addProfile(let parameters):
            return parameters
        case .login(let parameters):
            return parameters
        case .forgetPassword(let parameters):
            return parameters
        case .verificationCode(let parameters):
            return parameters
        case .newPassword(let parameters):
            return parameters
        }
    }
    
    var url: URL {
        let relativePath: String
        switch self {
        case .addProfile:
            relativePath = Constants.addProfile
        case .login:
            relativePath = Constants.login
        case .forgetPassword:
            relativePath = Constants.forgetPassword
        case .verificationCode:
            relativePath = Constants.verificationCode
        case .newPassword:
            relativePath = Constants.newPassword
        }
        
        return URL(string: Constants.baseUrl)!.appendingPathComponent(relativePath)
    }
    
    var encoding: ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
}
