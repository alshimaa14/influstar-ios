//
//  SideMenuViewController.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SideMenuViewController: BaseTableViewController<Menu, SideMenuPresenter, SideMenuCell>, SideMenuViewProtocol {
    
    private let mainView = SideMenuView()
    var presenter: SideMenuPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
        basePresenter = presenter as? SideMenuPresenter
        baseMainView = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
        
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
    func reloadTableView() {
        mainView.tableView.reloadData()
    }
    
}
