//
//  AboutViewController.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController, AboutViewProtocol {
    
    private let mainView = AboutView()
    var presenter: AboutPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
