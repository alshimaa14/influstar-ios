//
//  ScanCodeRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ScanCodeRouter: BaseRouter, ScanCodeRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = ScanCodeViewController()
        let interactor = ScanCodeInteractor()
        let router = ScanCodeRouter()
        let presenter = ScanCodePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
}
