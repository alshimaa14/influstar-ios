//
//  AuthenticationWorker.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AuthenticationWorker {
    
    private let networkHandler = NetworkHandler()
    
    func addProfile(parameters: [String: Any], completionHandler: @escaping(Result<User>) -> ()) {
        let addProfileRequest = AuthenticationRouter.addProfile(parameters: parameters)
        do {
            try networkHandler.request(addProfileRequest).decoded(toType: User.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func login(parameters: [String: Any], completionHandler: @escaping(Result<User>) -> ()) {
        let loginRequest = AuthenticationRouter.login(parameters: parameters)
        do {
            try networkHandler.request(loginRequest).decoded(toType: User.self).observe(with: completionHandler)
        } catch {
            completionHandler(.failure(error))
        }
    }
    
    func forgetPassword(parameters: [String: Any], completionHandler: @escaping(Result<Data>) -> ()) {
        let forgetPasswordRequest = AuthenticationRouter.forgetPassword(parameters: parameters)
        networkHandler.request(forgetPasswordRequest).observe(with: completionHandler)
    }
    
    func getVerificationCode(parameters: [String: Any], completionHandler: @escaping(Result<Data>) -> ()) {
        let verificationCodeRequest = AuthenticationRouter.verificationCode(parameters: parameters)
        networkHandler.request(verificationCodeRequest).observe(with: completionHandler)
    }
    
    func newPassword(parameters: [String: Any], completionHandler: @escaping(Result<Data>) -> ()) {
        let newPasswordRequest = AuthenticationRouter.newPassword(parameters: parameters)
        networkHandler.request(newPasswordRequest).observe(with: completionHandler)
    }
}
