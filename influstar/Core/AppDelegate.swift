//
//  AppDelegate.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    private var rootViewController: UINavigationController? {
        return (UIApplication.shared.keyWindow?.rootViewController as? CustomNavigationController)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.backgroundColor = .white
        IQKeyboardManager.shared.enable = true
       
//        startApplication()
        window?.rootViewController = CustomNavigationController(rootViewController: LoginRouter.createModule())

        return true
    }
    
    func startApplication() {
        if UserDefaultsHelper.isFirstOpenApp {
            setRootViewController(MyProfileRouter.createModule(), animated: false)
        } else {
            setRootViewController(ChooseLanguageRouter.createModule(), animated: false)
            UserDefaultsHelper.isFirstOpenApp = true
        }
    }
    
    func setRootViewController(_ viewController: UIViewController, animated: Bool) {

        if animated {
            let transition = UIView.AnimationOptions.transitionCrossDissolve
            window?.rootViewController = CustomNavigationController(rootViewController: viewController)
            UIView.transition(with: window!, duration: 0.5, options: transition, animations: {})
        } else {
            window?.rootViewController = CustomNavigationController(rootViewController: viewController)
        }
        
    }
}

