//
//  VerifyCodeViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class VerifyCodeViewController: BaseViewController, VerifyCodeViewProtocol {
    
    private let mainView = VerifyCodeView()
    var presenter: VerifyCodePresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "verificationCode".localized
        addTargets()
    }

    private func addTargets() {
        [mainView.firstDigitTextField, mainView.secondDigitTextField, mainView.thirdDigitTextField, mainView.fourthDigitTextField].forEach {
            $0.addTarget(self, action: #selector(handleNextTextField(textField:)), for: .editingChanged)
        }
        
        mainView.verifyButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.verifyCode(firstDigit: self.mainView.firstDigitTextField.text!, secondDigit: self.mainView.secondDigitTextField.text!, thirdDigit: self.mainView.thirdDigitTextField.text!, forthDigit: self.mainView.fourthDigitTextField.text!)
        }
        mainView.backButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.dismissViewController()
        }
    }
    
    @objc private func handleNextTextField(textField: UITextField) {
        if textField.text!.count >= 1 {
            let lastCharacter = String(textField.text!.last!)
            textField.text = lastCharacter
        }
        
        guard !textField.text!.isEmpty else { return }
        
        switch textField {
        case mainView.firstDigitTextField:
            mainView.secondDigitTextField.becomeFirstResponder()
            
        case mainView.secondDigitTextField:
            mainView.thirdDigitTextField.becomeFirstResponder()
            
        case mainView.thirdDigitTextField:
            mainView.fourthDigitTextField.becomeFirstResponder()
            
        case mainView.fourthDigitTextField:
            view.endEditing(true)
            
        default:
            break
        }
    }
}
