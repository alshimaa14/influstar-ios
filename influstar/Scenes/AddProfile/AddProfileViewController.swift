//
//  AddProfileViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AddProfileViewController: BaseViewController, AddProfileViewProtocol {
    
    private let mainView = AddProfileView()
    var presenter: AddProfilePresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "addProfile".localized
        addTargets()
    }
    
    private func addTargets() {
        mainView.registerButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.register(userName: self.mainView.usernameTextField.text, email: self.mainView.emailTextField.text, password: self.mainView.passwordTextField.text, confirmPassword: self.mainView.confirmPasswordTextField.text, isAcceptedConditions: self.mainView.termsAndConditionsButton.isChecked)
        }
        mainView.backButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.dismissViewController()
        }
        mainView.profileButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.navigateToLogin()
        }
    }
    
}
