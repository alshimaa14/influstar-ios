//
//  File.swift
//  influstar
//
//  Created by ibtdi.com on 8/18/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class Social {
    let image: String?
    let color: UIColor?
    let hasLink: Bool = false
    
    init(image: String, color: UIColor) {
        self.image = image
        self.color = color
    }
}
