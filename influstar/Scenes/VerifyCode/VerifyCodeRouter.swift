//
//  VerifyCodeRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class VerifyCodeRouter: BaseRouter, VerifyCodeRouterProtocol {
    
    static func createModule(email: String) -> UIViewController {
        let view = VerifyCodeViewController()
        let interactor = VerifyCodeInteractor()
        let router = VerifyCodeRouter()
        let presenter = VerifyCodePresenter(view: view, interactor: interactor, router: router, email: email)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToNewPassword(code: String) {
        self.viewController?.navigationController?.pushViewController(NewPasswordRouter.createModule(code: code), animated: true)
    }
    
    func dismiss() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
