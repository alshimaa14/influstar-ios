//
//  Menu.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class Menu {
    var image: String = ""
    var title: String = ""
    var isLasetItem: Bool = false
    
    init(image: String, title: String, isLastItem: Bool) {
        self.image = image
        self.title = title
        self.isLasetItem = isLastItem
    }
}
