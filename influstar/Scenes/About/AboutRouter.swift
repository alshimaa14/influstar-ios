//
//  AboutRouter.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AboutRouter: BaseRouter, AboutRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = AboutViewController()
        let interactor = AboutInteractor()
        let router = AboutRouter()
        let presenter = AboutPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
}
