//
//  AddProfileInteractor.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class AddProfileInteractor: AddProfileInteractorInputProtocol {
    
    weak var presenter: AddProfileInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func addProfile(parameters: [String : Any]) {
        authenticationWorker.addProfile(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didFetchAddProfile(with: result)
        }
    }
}
