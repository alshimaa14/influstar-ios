//
//  SideMenuPresenter.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class SideMenuPresenter: BaseListPresenter<Menu>, SideMenuPresenterProtocol, SideMenuInteractorOutputProtocol {
    
    weak var view: SideMenuViewProtocol?
    private let interactor: SideMenuInteractorInputProtocol
    private let router: SideMenuRouterProtocol
    
    override var numberOfRows: Int {
        return items.count
    }
    
    init(view: SideMenuViewProtocol, interactor: SideMenuInteractorInputProtocol, router: SideMenuRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        self.items.append(contentsOf: [Menu(image: "ic_home", title: "home".localized, isLastItem: false),
                                       Menu(image: "ic_help", title: "help".localized, isLastItem: false),
                                       Menu(image: "ic_about", title: "about".localized, isLastItem: false),
                                       Menu(image: "ic_data_policy", title: "dataPolicy".localized, isLastItem: false),
                                       Menu(image: "ic_terms_of_use", title: "termsOfUse".localized, isLastItem: false),
                                       Menu(image: "ic_report", title: "reportAProblem".localized, isLastItem: false)])
        if UserDefaultsHelper.isLoggedIn {
            items.append(Menu(image: "ic_logout", title: "Logout".localized, isLastItem: true))
        } else {
            self.items.append(Menu(image: "", title: "Login".localized, isLastItem: true))
        }
    }
    
    override func didSelectRow(at indexPath: IndexPath) {
        if indexPath.row == 6 && UserDefaultsHelper.isLoggedIn {
            UserDefaultsHelper.isLoggedIn = false
            view?.reloadTableView()
            router.navigateToItems(to: .login)
        } else if indexPath.row == 6 && !UserDefaultsHelper.isLoggedIn {
            view?.reloadTableView()
            router.navigateToItems(to: .login)
        } else {
            router.navigateToItems(to: .about)
        }
    }
    
}
