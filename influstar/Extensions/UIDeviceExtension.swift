//
//  UIDeviceExtension.swift
//  influstar
//
//  Created by ibtdi.com on 8/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIDevice {
    public class var hasTopNotch: Bool {
        guard let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
            return false
        }
        return true
    }
}
