//
//  HomeButton.swift
//  influstar
//
//  Created by Vortex on 7/31/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class HomeButton: UIButton {
    
    static func create() -> HomeButton {
        let button = HomeButton(type: .system)
        button.initialize()
        return button
    }
    
    private func initialize() {
        backgroundColor = .white
        titleLabel?.font = AvenirNextFont.demi.getFont(ofSize: 14)
        setTitleColor(.appColor, for: .normal)
        constraint {
            $0.height(46)
            $0.width(140)
        }
        layer.cornerRadius = 23
        layer.applySketchShadow(color: .rgba(91, 83, 206, 0.07), alpha: 1.0, x: 0, y: 8, blur: 10, spread: 0)
        
    }
    
}
