//
//  TextField.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class TextField: UITextField, UITextFieldDelegate {
    
    private weak var _delegate: UITextFieldDelegate?
    override var delegate: UITextFieldDelegate? {
        get {
            return _delegate
        } set {
            return _delegate = newValue
        }
    }
    
    var characterLimit = 0
    
    private let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .rgba(219, 218, 218, 1)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        font = AvenirNextFont.regular.getFont(ofSize: 13)
        super.delegate = self
        smartInsertDeleteType = .no
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addSubviews() {
        addSubview(lineView)
    }
    
    private func setupLineViewConstraints() {
        lineView.constraint {
            $0.leading(0)
            $0.trailing(0)
            $0.centerX(0)
            $0.topToBottom(ofView: self, withPadding: 0)
            $0.height(1)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupLineViewConstraints()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return _delegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        _delegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return _delegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       _delegate?.textFieldDidEndEditing?(textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        _delegate?.textFieldDidEndEditing?(textField, reason: reason)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if characterLimit != 0 {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= characterLimit
        }
        return _delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return _delegate?.textFieldShouldClear?(textField) ?? true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return _delegate?.textFieldShouldReturn?(textField) ?? true
    }
}
