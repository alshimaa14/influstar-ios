//
//  ForgetPasswordRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ForgetPasswordRouter: BaseRouter, ForgetPasswordRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = ForgetPasswordViewController()
        let interactor = ForgetPasswordInteractor()
        let router = ForgetPasswordRouter()
        let presenter = ForgetPasswordPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToVerificationCode(email: String) {
        self.viewController?.navigationController?.pushViewController(VerifyCodeRouter.createModule(email: email), animated: true)
    }
    
    func dismiss() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
}
