//
//  ClosureSleeve.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

@objc class ClosureSleeve: NSObject {
    let closure: () -> ()
    
    init (_ closure: @escaping () -> ()) {
        self.closure = closure
        super.init()
    }
    
    @objc func invoke() {
        closure()
    }
}
