//
//  UIButtonExtension.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIButton {
    func onTap(_ closure: @escaping () -> ()) {
        addAction(for: .touchUpInside, closure)
    }
}
