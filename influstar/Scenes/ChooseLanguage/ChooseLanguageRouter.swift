//
//  ChooseLanguageRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChooseLanguageRouter: BaseRouter, ChooseLanguageRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = ChooseLanguageViewController()
        let interactor = ChooseLanguageInteractor()
        let router = ChooseLanguageRouter()
        let presenter = ChooseLanguagePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToProfile(language: Language) {
        if language == .arabic {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
//        self.viewController?.navigationController?.pushViewController(LoginRouter.createModule(), animated: true)
        AppDelegate.shared.setRootViewController(
            LoginRouter.createModule(),
            animated: true
        )
    }
    
}
