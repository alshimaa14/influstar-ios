//
//  NSObjectExtension.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

extension NSObject {
    
    class var className: String {
        return "\(self)"
    }
    
}
