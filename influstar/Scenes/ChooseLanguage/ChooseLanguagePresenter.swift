//
//  ChooseLanguagePresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ChooseLanguagePresenter: ChooseLanguagePresenterProtocol, ChooseLanguageInteractorOutputProtocol {
    
    weak var view: ChooseLanguageViewProtocol?
    private let interactor: ChooseLanguageInteractorInputProtocol
    private let router: ChooseLanguageRouterProtocol
    
    init(view: ChooseLanguageViewProtocol, interactor: ChooseLanguageInteractorInputProtocol, router: ChooseLanguageRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func navigateToProfile(language: Language) {
//        L102Language.setLanguage(code: language.rawValue)
        router.navigateToProfile(language: language)
    }
    
}
