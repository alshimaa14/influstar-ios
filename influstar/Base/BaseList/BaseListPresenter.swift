//
//  BaseTableViewPresenter.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol BaseListPresenterProtocol: class {
    
    associatedtype Model
    
    var numberOfRows: Int { get }
    func configure(cell: AnyConfigurableCell<Model>, at indexPath: IndexPath)
    func didSelectRow(at indexPath: IndexPath)
    func loadNextPage()
    func pullToRefresh()
}

class BaseListPresenter<Model>: BaseListPresenterProtocol {
    
    weak var baseView: BaseListViewProtocol?
    
    var isPullToRefresh = false {
        didSet {
            if !isPullToRefresh {
                baseView?.endRefreshing()
            }
        }
    }
    
    var items = [Model]() {
        didSet {
            baseView?.reloadData()
        }
    }
    
    var numberOfRows: Int {
        return items.count
    }
    
    func configure(cell: AnyConfigurableCell<Model>, at indexPath: IndexPath) {
        cell.configure(model: items[indexPath.row])
    }
    
    func pullToRefresh() {
        
    }
    
    func loadNextPage() {
        
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        
    }
}

