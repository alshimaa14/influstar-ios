//
//  NewPasswordRouter.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordRouter: BaseRouter, NewPasswordRouterProtocol {
    
    static func createModule(code: String) -> UIViewController {
        let view = NewPasswordViewController()
        let interactor = NewPasswordInteractor()
        let router = NewPasswordRouter()
        let presenter = NewPasswordPresenter(view: view, interactor: interactor, router: router, code: code)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigateToLogin() {
        viewController?.navigationController?.pushViewController(LoginRouter.createModule(), animated: true)
    }
    
    func dismiss() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
}
