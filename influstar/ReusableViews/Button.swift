//
//  Button.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/28/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class Button: UIButton {
    
    static func create() -> Button {
        let button = Button(type: .system)
        button.initialize()
        return button
    }
    
    private func initialize() {
        backgroundColor = .appColor
        titleLabel?.font = AvenirNextFont.demi.getFont(ofSize: 14)
        setTitleColor(.white, for: .normal)
        constraint {
            $0.height(50)
        }
        layer.cornerRadius = 25
    }
    
}
