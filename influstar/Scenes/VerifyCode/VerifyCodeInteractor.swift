//
//  VerifyCodeInteractor.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class VerifyCodeInteractor: VerifyCodeInteractorInputProtocol {
    
    weak var presenter: VerifyCodeInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func verifyCode(parameters: [String: Any], code: String) {
        authenticationWorker.getVerificationCode(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didVerifyCode(with: result, code: code)
        }
    }
}
