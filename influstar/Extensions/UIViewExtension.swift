//
//  UIViewExtension.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIView {
    
    func constraint(block: (UIView) -> ()) {
        translatesAutoresizingMaskIntoConstraints = false
        block(self)
    }
    
    
    @discardableResult func safeAreaTop(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = topAnchor.constraint(equalTo: constraintToView.safeAreaLayoutGuide.topAnchor, constant: padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func top(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = topAnchor.constraint(equalTo: constraintToView.topAnchor, constant: padding)
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func topToBottom(ofView view: UIView, withPadding padding: CGFloat) -> NSLayoutConstraint {
        let constraint = topAnchor.constraint(equalTo: view.bottomAnchor, constant: padding)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func topToCenterY(ofView view: UIView, withPadding padding: CGFloat) -> NSLayoutConstraint {
        let constraint = topAnchor.constraint(equalTo: view.centerYAnchor, constant: padding)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func leading(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = leadingAnchor.constraint(equalTo: constraintToView.leadingAnchor, constant: padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func leadingToTrailing(ofView view: UIView, withPadding padding: CGFloat) -> NSLayoutConstraint {
        let constraint = leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: padding)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func leadingToCenterX(ofView view: UIView, withPadding padding: CGFloat) -> NSLayoutConstraint {
        let constraint = leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: padding)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func trailing(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = trailingAnchor.constraint(equalTo: constraintToView.trailingAnchor, constant: -padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func trailingToCenterX(ofView view: UIView, withPadding padding: CGFloat) -> NSLayoutConstraint {
        let constraint = trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: padding)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func bottom(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = bottomAnchor.constraint(equalTo: constraintToView.bottomAnchor, constant: -padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func safeAreaBottom(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = bottomAnchor.constraint(equalTo: constraintToView.safeAreaLayoutGuide.bottomAnchor, constant: -padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func bottomToTop(ofView view: UIView, withPadding padding: CGFloat) -> NSLayoutConstraint {
        let constraint = bottomAnchor.constraint(equalTo: view.topAnchor, constant: -padding)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func centerX(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = centerXAnchor.constraint(equalTo: constraintToView.centerXAnchor, constant: padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    
    @discardableResult func centerY(_ padding: CGFloat, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = centerYAnchor.constraint(equalTo: constraintToView.centerYAnchor, constant: padding)
        
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func height(_ constant: CGFloat) -> NSLayoutConstraint {
        let constraint = heightAnchor.constraint(equalToConstant: constant)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func width(_ constant: CGFloat) -> NSLayoutConstraint {
        let constraint = widthAnchor.constraint(equalToConstant: constant)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult func height(multiplier: CGFloat, constant: CGFloat = 0, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = heightAnchor.constraint(equalTo: constraintToView.heightAnchor, multiplier: multiplier, constant: constant)
        constraint.isActive = true
        return constraint
        
    }
    
    @discardableResult func width(multiplier: CGFloat, constant: CGFloat = 0, toView view: UIView? = nil) -> NSLayoutConstraint {
        
        let constraint: NSLayoutConstraint
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        constraint = widthAnchor.constraint(equalTo: constraintToView.widthAnchor, multiplier: multiplier, constant: constant)
        constraint.isActive = true
        return constraint
        
    }
    
    func pin(edges: Edge..., toView view: UIView? = nil) {
        
        let constraintToView: UIView
        
        if let view = view {
            constraintToView = view
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            constraintToView = superview
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        for edge in edges {
            switch edge {
            case .safeAreaTop(let padding):
                safeAreaTop(padding, toView: constraintToView)
            case .top(let padding):
                top(padding, toView: constraintToView)
            case .leading(let padding):
                leading(padding, toView: constraintToView)
            case .trailing(let padding):
                trailing(padding, toView: constraintToView)
            case .bottom(let padding):
                bottom(padding, toView: constraintToView)
            case .safeAreaBottom(let padding):
                safeAreaBottom(padding, toView: constraintToView)
            }
        }
    }
    
    func pinAllEdges(padding: CGFloat = 0, toView view: UIView? = nil) {
        translatesAutoresizingMaskIntoConstraints = false
        if let view = view {
            top(padding, toView: view)
            leading(padding, toView: view)
            trailing(padding, toView: view)
            bottom(padding, toView: view)
        } else {
            guard let superview = superview else { fatalError("Both view and superview are nil") }
            top(padding, toView: superview)
            leading(padding, toView: superview)
            trailing(padding, toView: superview)
            bottom(padding, toView: superview)
        }
    }
    
    func attachTapGesture(_ handler: @escaping () -> ()) {
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer()
        tapGesture.handler(handler)
        addGestureRecognizer(tapGesture)
    }
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.isUserInteractionEnabled = !isUserInteractionEnabled
        hud.restorationIdentifier = "activityIndicator"
    }
    
    func hideActivityIndicator() {
        for subview in subviews where subview.restorationIdentifier == "activityIndicator" {
            guard let hud = subview as? MBProgressHUD else { return }
            hud.hide(animated: true)
        }
    }
    
    func fadeIn(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 1.0, completionHandler: @escaping () -> ()) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) { _ in
            completionHandler()
        }
    }
    
    func remove(fadeOutDuration: Double) {
        fadeOut(duration: 0.25) {
            self.removeFromSuperview()
        }
    }
}
