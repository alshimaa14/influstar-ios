//
//  ForgetPasswordViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController, ForgetPasswordViewProtocol {
    
    private let mainView = ForgetPasswordView()
    var presenter: ForgetPasswordPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
        addTargets()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "forgotPassword".localized
    }
    
    private func addTargets() {
        mainView.sendButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.forgetPassword(email: self.mainView.emailTextField.text)
        }
        mainView.backButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.dismissViewController()
        }
    }
    
}
