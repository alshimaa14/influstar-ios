//
//  ForgetPasswordPresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ForgetPasswordPresenter: ForgetPasswordPresenterProtocol, ForgetPasswordInteractorOutputProtocol {
    
    weak var view: ForgetPasswordViewProtocol?
    private let interactor: ForgetPasswordInteractorInputProtocol
    private let router: ForgetPasswordRouterProtocol
    
    init(view: ForgetPasswordViewProtocol, interactor: ForgetPasswordInteractorInputProtocol, router: ForgetPasswordRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func forgetPassword(email: String) {
        guard !email.isEmpty else {
            router.showAlert(with: "emptyEmail".localized)
            return
        }
        
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        let parameter: [String: Any] =  ["email": email]
        interactor.forgetPassword(parameters: parameter, email: email)
    }
    
    func didForgetPassword(with result: Result<Data>, email: String) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            router.navigateToVerificationCode(email: email)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func dismissViewController() {
        router.dismiss()
    }
}
