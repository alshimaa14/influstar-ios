//
//  MyProfileViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class MyProfileViewController: BaseCollectionViewController<Social, MyProfilePresenter, SocialMediaCell>, MyProfileViewProtocol {
    
    private let mainView = MyProfileView()
    var presenter: MyProfilePresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
        basePresenter = presenter as? MyProfilePresenter
        baseMainView = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "tareksabry1337"
        DispatchQueue.main.async {
            self.presenter.viewDidLoad()
        }
        
        setupNavigationBarButtons()
        addTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    private func setupNavigationBarButtons() {
        let menuButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .image(UIImage(named: "ic_menu"))
            .height(25)
            .width(25)
            .action(target: self, selector: #selector(menuButtonTapped))
            .build()
        
        navigationItem.rightBarButtonItem = menuButton
    }
    
    @objc private func menuButtonTapped() {
        self.presenter.openMenu()
    }
    
    private func addTargets() {
        mainView.addMoreButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.addMoreButtonTapped()
        }
        mainView.plusButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.plusButtonTapped()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width - 50) / 2
        return .init(width: width, height: 130)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 17
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 30, left: 19, bottom: 30, right: 19)
    }
    
    override func reloadData() {
        super.reloadData()
        mainView.collectionViewHeightConstraint.constant = mainView.collectionView.collectionViewLayout.collectionViewContentSize.height
    }
}
