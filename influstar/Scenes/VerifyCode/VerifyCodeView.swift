//
//  VerifyCodeView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class VerifyCodeView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "bg"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "white_back"), for: .normal)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 18)
        label.text = "verificationCode".localized
        label.textColor = .white
        return label
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        return UIView()
    }()
    
    private lazy var headImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "male_head"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 17
        return view
    }()
    
    private lazy var feetImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "male_feet"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var enterCodeLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 15)
        label.textColor = .white
        label.text = "pleaseEnterCode".localized
        return label
    }()
    
    lazy var firstDigitTextField: TextField = {
        let textField = TextField()
        textField.textAlignment = .center
        textField.constraint {
            $0.width(40)
            $0.height(30)
        }
        return textField
    }()
    
    lazy var secondDigitTextField: TextField = {
        let textField = TextField()
        textField.textAlignment = .center
        return textField
    }()
    
    lazy var thirdDigitTextField: TextField = {
        let textField = TextField()
        textField.textAlignment = .center
        return textField
    }()
    
    lazy var fourthDigitTextField: TextField = {
        let textField = TextField()
        textField.textAlignment = .center
        return textField
    }()
    
    private lazy var inputsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [firstDigitTextField, secondDigitTextField, thirdDigitTextField, fourthDigitTextField])
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 20
        return stackView
    }()
    
    lazy var verifyButton: Button = {
        let button = Button.create()
        button.setTitle("verify".localized, for: .normal)
        return button
    }()
    
    lazy var resendButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = AvenirNextFont.demi.getFont(ofSize: 13)
        button.setTitleColor(.appColor, for: .normal)
        button.setTitle("resendCode".localized, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(backButton)
        addSubview(titleLabel)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(inputsContainerView)
        containerView.addSubview(headImageView)
        containerView.addSubview(feetImageView)
        containerView.addSubview(enterCodeLabel)
        inputsContainerView.addSubview(inputsStackView)
        inputsContainerView.addSubview(verifyButton)
        inputsContainerView.addSubview(resendButton)
    }
    
    private func setupBackgroundImageViewConstarints() {
        backgroundImageView.pinAllEdges()
    }
    
    private func setupBackButtonConstrainsts() {
        backButton.constraint {
            $0.top(UIDevice.hasTopNotch ? 55 : 50)
            $0.leading(18)
            $0.width(20)
            $0.height(20)
        }
    }
    
    private func setupTitleLabelConstraints() {
        titleLabel.constraint {
            $0.centerX(0)
            $0.centerY(0, toView: backButton)
        }
    }
    
    private func setupScrollViewConstraints() {
        scrollView.constraint {
            $0.topToBottom(ofView: backButton, withPadding: 0)
            $0.bottom(0)
            $0.leading(0)
            $0.trailing(0)
        }
    }
    
    private func setupContainerViewConstraints() {
        containerView.pinAllEdges()
        containerView.constraint {
            $0.width(multiplier: 1.0)
        }
    }
    
    private func setupHeadImageViewConstraints() {
        headImageView.constraint {
            $0.top(-2)
            $0.trailing(0, toView: inputsContainerView)
        }
    }
    
    private func setupInputsContainerViewConstraints() {
        inputsContainerView.constraint {
            $0.topToBottom(ofView: headImageView, withPadding: 0)
            $0.leading(16)
            $0.trailing(16)
            $0.bottom(16)
        }
    }
    
    private func setupFeetImageViewConstraints() {
        feetImageView.constraint {
            $0.topToBottom(ofView: headImageView, withPadding: 0)
            $0.trailing(0, toView: headImageView)
        }
    }
    
    private func setupEnterCodeLabelConstraints() {
        enterCodeLabel.constraint {
            $0.leading(16, toView: inputsContainerView)
            $0.bottomToTop(ofView: inputsContainerView, withPadding: 8)
        }
    }
    
    private func setupInputsStackViewConstraints() {
        inputsStackView.constraint {
            $0.topToBottom(ofView: feetImageView, withPadding: 16)
            $0.centerX(0)
        }
    }
    
    private func setupVerifyButtonConstraints() {
        verifyButton.constraint {
            $0.topToBottom(ofView: inputsStackView, withPadding: 43.5)
            $0.leading(26)
            $0.trailing(26)
        }
    }
    
    private func setupResendButtonConstraints() {
        resendButton.constraint {
            $0.topToBottom(ofView: verifyButton, withPadding: 18)
            $0.centerX(0)
            $0.bottom(27)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstarints()
        setupBackButtonConstrainsts()
        setupTitleLabelConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupHeadImageViewConstraints()
        setupInputsContainerViewConstraints()
        setupFeetImageViewConstraints()
        setupInputsStackViewConstraints()
        setupEnterCodeLabelConstraints()
        setupVerifyButtonConstraints()
        setupResendButtonConstraints()
    }
    
}
