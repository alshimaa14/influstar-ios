//
//  LoginInteractor.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LoginInteractor: LoginInteractorInputProtocol {
    
    weak var presenter: LoginInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func login(parameters: [String : Any]) {
        authenticationWorker.login(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didLogin(with: result)
        }
    }
}
