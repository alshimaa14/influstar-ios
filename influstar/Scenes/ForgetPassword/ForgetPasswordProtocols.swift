//
//  ForgetPasswordProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ForgetPasswordRouterProtocol: BaseRouterProtocol {
    func navigateToVerificationCode(email: String)
    func dismiss()
}

protocol ForgetPasswordPresenterProtocol: class {
    var view: ForgetPasswordViewProtocol? { get set }
    func forgetPassword(email: String)
    func dismissViewController()
}

protocol ForgetPasswordInteractorInputProtocol: class {
    var presenter: ForgetPasswordInteractorOutputProtocol? { get set }
    func forgetPassword(parameters: [String: Any], email: String)
}

protocol ForgetPasswordInteractorOutputProtocol: class {
    func didForgetPassword(with result: Result<Data>, email: String)
}

protocol ForgetPasswordViewProtocol: HasActivityIndicator {
    var presenter: ForgetPasswordPresenterProtocol! { get set }
}
