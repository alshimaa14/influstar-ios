//
//  AvenirNextFont.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum AvenirNextFont: String {
    
    case heavyCnIt
    case cn
    case demiCnIt
    case demiIt
    case heavyCn
    case boldCn
    case boldCnIt
    case ultLtCn
    case heavy
    case mediumCnIt
    case ultLtCnIt
    case bold
    case demi
    case mediumIt
    case regular
    case it
    case demiCn
    case heavyIt
    case ultLt
    case mediumCn
    case ultLtIt
    case cnIt
    
    func getFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "AvenirNextLTPro-\(self.rawValue.capitalized)", size: size) ?? .systemFont(ofSize: size)
    }
    
}
