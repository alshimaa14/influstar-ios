//
//  ScanCodeProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol ScanCodeRouterProtocol: BaseRouterProtocol {
    
}

protocol ScanCodePresenterProtocol: class {
    var view: ScanCodeViewProtocol? { get set }
}

protocol ScanCodeInteractorInputProtocol: class {
    var presenter: ScanCodeInteractorOutputProtocol? { get set }
}

protocol ScanCodeInteractorOutputProtocol: class {
    
}

protocol ScanCodeViewProtocol: class {
    var presenter: ScanCodePresenterProtocol! { get set }
}
