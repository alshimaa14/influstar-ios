//
//  NewPasswordInteractor.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NewPasswordInteractor: NewPasswordInteractorInputProtocol {
    
    weak var presenter: NewPasswordInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func newPassword(parameters: [String : Any]) {
        authenticationWorker.newPassword(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didAddNewPassword(with: result)
        }
    }
}
