//
//  NewPasswordViewController.swift
//  influstar
//
//  Created by ibtdi.com on 8/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//
import UIKit

class NewPasswordViewController: BaseViewController, NewPasswordViewProtocol {
    
    private let mainView = NewPasswordView()
    var presenter: NewPasswordPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "newPassword".localized
        addTargets()
    }
    
    private func addTargets() {
        mainView.sendButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.newPassword(password: self.mainView.passwordTextField.text, confirmPaswword: self.mainView.confirmPasswordTextField.text)
        }
        mainView.backButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.dismissViewController()
        }
    }
    
}
