//
//  ForgetPasswordInteractor.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ForgetPasswordInteractor: ForgetPasswordInteractorInputProtocol {
    
    weak var presenter: ForgetPasswordInteractorOutputProtocol?
    private let authenticationWorker = AuthenticationWorker()
    
    func forgetPassword(parameters: [String : Any], email: String) {
        authenticationWorker.forgetPassword(parameters: parameters) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.didForgetPassword(with: result, email: email)
        }
    }
}
