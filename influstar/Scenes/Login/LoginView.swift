//
//  LoginView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "bg"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "white_back"), for: .normal)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 18)
        label.text = "haveAProfile".localized
        label.textColor = .white
        return label
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        return UIView()
    }()
    
    private lazy var inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 17
        return view
    }()
    
    private lazy var enterEmailLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 15)
        label.textColor = .white
        label.text = "pleaseEnterYourCredentials".localized
        return label
    }()
    
    lazy var emailTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "email".localized
        return textField
    }()
    
    lazy var passwordTextField: LabelTextField = {
        let textField = LabelTextField()
        textField.topLabel.text = "password".localized
        textField.isSecureTextEntry = true
        return textField
    }()
    
    lazy var forgotPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = AvenirNextFont.demi.getFont(ofSize: 13)
        button.setTitleColor(.appColor, for: .normal)
        button.setTitle("forgotPassword".localized, for: .normal)
        return button
    }()
    
    lazy var loginButton: Button = {
        let button = Button.create()
        button.setTitle("login".localized, for: .normal)
        return button
    }()
    
    lazy var doNotHaveAProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = AvenirNextFont.demi.getFont(ofSize: 13)
        button.setTitleColor(.appColor, for: .normal)
        button.setTitle("doNotHaveAProfile".localized, for: .normal)
        return button
    }()
    
    lazy var sittingGirlImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "sitting_girl"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(backButton)
        addSubview(titleLabel)
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(enterEmailLabel)
        containerView.addSubview(inputsContainerView)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(forgotPasswordButton)
        inputsContainerView.addSubview(loginButton)
        inputsContainerView.addSubview(doNotHaveAProfileButton)
        inputsContainerView.addSubview(sittingGirlImageView)
    }
    
    private func setupBackgroundImageViewConstarints() {
        backgroundImageView.pinAllEdges()
    }
    
    private func setupBackButtonConstrainsts() {
        backButton.constraint {
            $0.top(UIDevice.hasTopNotch ? 55 : 50)
            $0.leading(18)
            $0.width(40)
            $0.height(40)
        }
    }
    
    private func setupTitleLabelConstraints() {
        titleLabel.constraint {
            $0.centerX(0)
            $0.centerY(0, toView: backButton)
        }
    }
    
    private func setupScrollViewConstraints() {
        scrollView.constraint {
            $0.topToBottom(ofView: backButton, withPadding: 0)
            $0.bottom(0)
            $0.leading(0)
            $0.trailing(0)
        }
    }
    
    private func setupContainerViewConstraints() {
        containerView.pinAllEdges()
        containerView.constraint {
            $0.width(multiplier: 1.0)
        }
    }
    
    private func setupEnterEmailLabelConstraints() {
        enterEmailLabel.constraint {
            $0.top(90)
            $0.leading(16, toView: inputsContainerView)
        }
    }
    
    private func setupInputsContainerViewConstraints() {
        inputsContainerView.constraint {
            $0.topToBottom(ofView: enterEmailLabel, withPadding: 12)
            $0.leading(16)
            $0.trailing(16)
            $0.bottom(16)
        }
    }
    
    private func setupEmailTextFieldConstraints() {
        emailTextField.constraint {
            $0.top(48)
            $0.leading(28)
            $0.trailing(28)
        }
    }
    
    private func setupPasswordTextFieldConstraints() {
        passwordTextField.constraint {
            $0.topToBottom(ofView: emailTextField, withPadding: 10.5)
            $0.leading(28)
            $0.trailing(28)
        }
    }
    
    private func setupForgotPasswordButtonConstraints() {
        forgotPasswordButton.constraint {
            $0.topToBottom(ofView: passwordTextField, withPadding: 15)
            $0.centerX(0)
        }
    }
    
    private func setupLoginButtonConstraints() {
        loginButton.constraint {
            $0.topToBottom(ofView: forgotPasswordButton, withPadding: 15)
            $0.leading(26)
            $0.trailing(26)
        }
    }
    
    private func setupDoNotHaveAProfileButtonConstraints() {
        doNotHaveAProfileButton.constraint {
            $0.topToBottom(ofView: loginButton, withPadding: 20)
            $0.centerX(0)
        }
    }
    
    private func setupSittingGirlImageViewConstraints() {
        sittingGirlImageView.constraint {
            $0.top(-6, toView: doNotHaveAProfileButton)
            $0.trailing(-2)
            $0.bottom(-8)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstarints()
        setupBackButtonConstrainsts()
        setupTitleLabelConstraints()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupEnterEmailLabelConstraints()
        setupInputsContainerViewConstraints()
        setupEmailTextFieldConstraints()
        setupPasswordTextFieldConstraints()
        setupForgotPasswordButtonConstraints()
        setupLoginButtonConstraints()
        setupDoNotHaveAProfileButtonConstraints()
        setupSittingGirlImageViewConstraints()
    }
    
}
