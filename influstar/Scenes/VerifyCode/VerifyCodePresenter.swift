//
//  VerifyCodePresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class VerifyCodePresenter: VerifyCodePresenterProtocol, VerifyCodeInteractorOutputProtocol {
    
    weak var view: VerifyCodeViewProtocol?
    private let interactor: VerifyCodeInteractorInputProtocol
    private let router: VerifyCodeRouterProtocol
    private var email: String
    
    init(view: VerifyCodeViewProtocol, interactor: VerifyCodeInteractorInputProtocol, router: VerifyCodeRouterProtocol, email: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.email = email
    }
    
    func verifyCode(firstDigit: String, secondDigit: String, thirdDigit: String, forthDigit: String) {
        guard !firstDigit.isEmpty else {
            router.showAlert(with: "emptyCode".localized)
            return
        }
        guard !secondDigit.isEmpty else {
            router.showAlert(with: "emptyCode".localized)
            return
        }
        guard !thirdDigit.isEmpty else {
            router.showAlert(with: "emptyCode".localized)
            return
        }
        guard !forthDigit.isEmpty else {
            router.showAlert(with: "emptyCode".localized)
            return
        }
        
        let code = "\(firstDigit)\(secondDigit)\(thirdDigit)\(forthDigit)"
        
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        let parameter: [String: Any] =  ["email": email,
                                         "code": code]
        interactor.verifyCode(parameters: parameter, code: code)
    }
    
    func didVerifyCode(with result: Result<Data>, code: String) {
        view?.hideActivityIndicator()
        switch result {
        case .success:
            router.navigateToNewPassword(code: code)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func dismissViewController() {
        router.dismiss()
    }
}
