//
//  SideMenuCell.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SideMenuCell: BaseTableViewCell<Menu> {
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_home")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.demi.getFont(ofSize: 13)
        label.text = "Home"
        label.textColor = .black
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(iconImageView)
        addSubview(titleLabel)
    }
    
    private func setupIconImageViewConstraints() {
        iconImageView.constraint {
            $0.leading(17)
            $0.centerY(0)
            $0.width(15)
            $0.height(15)
        }
    }
    
    private func setupTitleLabelConstraints() {
        titleLabel.constraint {
            $0.leadingToTrailing(ofView: iconImageView, withPadding: 10.5)
            $0.centerY(0)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupIconImageViewConstraints()
        setupTitleLabelConstraints()
    }

    override func configure(model: Menu) {
        iconImageView.image = UIImage(named: model.image)
        titleLabel.text = model.title
        if model.isLasetItem {
            titleLabel.textColor = .appColor
        } else {
            titleLabel.textColor = .black
        }
    }
}
