//
//  SoicalMediaCell.swift
//  influstar
//
//  Created by Vortex on 7/31/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SocialMediaCell: BaseCollectionViewCell<Social> {
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "twitter"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var linkImage: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ic_link"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 21
        layer.masksToBounds = true
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(iconImageView)
        addSubview(linkImage)
    }
    
    private func setupIconImageViewConstraints() {
        iconImageView.constraint {
            $0.centerX(0)
            $0.centerY(0)
        }
    }
    
    private func setupLinkImageConstraints() {
        linkImage.constraint {
            $0.trailing(15)
            $0.bottom(15)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupIconImageViewConstraints()
        setupLinkImageConstraints()
    }
    
    override func configure(model: Social) {
        iconImageView.image = UIImage(named: model.image ?? "")
        backgroundColor = model.color
//        if model.hasLink {
//            linkImage.isHidden = false
//        } else {
//            linkImage.isHidden = true
//        }
    }
    
}
