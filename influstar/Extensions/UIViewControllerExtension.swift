//
//  UIViewControllerExtension.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(with message: String) {
        AlertBuilder(title: "", message: message, preferredStyle: .alert)
            .addAction(title: "OK", style: .default)
            .build()
            .show()
    }
    
    func show(popupView: UIView, on view: UIView?) {
        let popupView = popupView
        addPopupView(popupView: popupView, on: view)
    }
    
    func addPopupView(popupView: UIView, on view: UIView?) {
        guard let view = view else { return }
        popupView.alpha = 0
        view.addSubview(popupView)
        popupView.constraint {
            $0.top(0)
            $0.leading(0)
            $0.trailing(0)
            $0.bottom(0)
        }
        popupView.fadeIn(duration: 0.25)
    }
    
}
