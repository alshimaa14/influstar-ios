//
//  AddProfilePresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AddProfilePresenter: AddProfilePresenterProtocol, AddProfileInteractorOutputProtocol {
    
    weak var view: AddProfileViewProtocol?
    private let interactor: AddProfileInteractorInputProtocol
    private let router: AddProfileRouterProtocol
    
    private var user: User?
    
    init(view: AddProfileViewProtocol, interactor: AddProfileInteractorInputProtocol, router: AddProfileRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func register(userName: String, email: String, password: String, confirmPassword: String, isAcceptedConditions: Bool) {
        guard !userName.isEmpty else {
            router.showAlert(with: "emptyUserName".localized)
            return
        }

        guard !email.isEmpty else {
            router.showAlert(with: "emptyEmail".localized)
            return
        }

        guard !password.isEmpty else {
            router.showAlert(with: "emptyPassword".localized)
            return
        }

        guard !confirmPassword.isEmpty else {
            router.showAlert(with: "emptyConfirmPassword".localized)
            return
        }

        guard isAcceptedConditions else {
            router.showAlert(with: "notAcceptedConditions".localized)
            return
        }
        
        guard password == confirmPassword else {
            router.showAlert(with: "passwordsDoNotMatch".localized)
            return
        }
        
        guard NSPredicate(format: "SELF MATCHES %@", Constants.Regex.userName).evaluate(with: userName) else {
            router.showAlert(with: "notStrongUserName".localized)
            return
        }
        
        guard NSPredicate(format: "SELF MATCHES %@", Constants.Regex.email).evaluate(with: email) else  {
            router.showAlert(with: "invalidEmail".localized)
            return
        }
        
        guard NSPredicate(format: "SELF MATCHES %@", Constants.Regex.password).evaluate(with: password) else  {
            router.showAlert(with: "weakPassword".localized)
            return
        }

        let parameter: [String: Any] =  ["name": userName,
                                         "email": email,
                                         "password": password,
                                         "confirm_password": confirmPassword]
        
        view?.showActivityIndicator(isUserInteractionEnabled: false)
        interactor.addProfile(parameters: parameter)
    }
    
    func didFetchAddProfile(with result: Result<User>) {
        view?.hideActivityIndicator()
        switch result {
        case .success(let result):
            user = result
            UserDefaultsHelper.isLoggedIn = true
            router.navigate(to: .login)
        case .failure(let error):
            router.showAlert(with: error.localizedDescription)
        }
    }
    
    func navigateToLogin() {
        router.navigate(to: .login)
    }
    
    func dismissViewController() {
        router.dismiss()
    }
    
}
