//
//  LoginViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, LoginViewProtocol {
    
    private let mainView = LoginView()
    var presenter: LoginPresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "haveAProfile".localized
        navigationController?.navigationBar.isHidden = true
//        navigationController?.navigationBar.isTranslucent = true
//        navigationController?.navigationBar.tintColor = .white
        addTargets()
    }
    
    func addTargets() {
        mainView.loginButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.login(email: self.mainView.emailTextField.text, password: self.mainView.passwordTextField.text)
        }
        mainView.forgotPasswordButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.navigateToForgetPassword()
            
        }
        mainView.doNotHaveAProfileButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.navigateToAddProfile()
        }
        mainView.backButton.onTap { [weak self] in
            guard let self = self else { return }
            self.presenter.dismissViewController()
        }
    }
    
}
