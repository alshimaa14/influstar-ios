//
//  ScanCodePresenter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class ScanCodePresenter: ScanCodePresenterProtocol, ScanCodeInteractorOutputProtocol {
    
    weak var view: ScanCodeViewProtocol?
    private let interactor: ScanCodeInteractorInputProtocol
    private let router: ScanCodeRouterProtocol
    
    init(view: ScanCodeViewProtocol, interactor: ScanCodeInteractorInputProtocol, router: ScanCodeRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}
