//
//  MyProfileProtocols.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

protocol MyProfileRouterProtocol: BaseRouterProtocol {
    func openSideMenu()
}

protocol MyProfilePresenterProtocol: class {
    var view: MyProfileViewProtocol? { get set }
    func viewDidLoad()
    func addMoreButtonTapped()
    func plusButtonTapped()
    func openMenu()
}

protocol MyProfileInteractorInputProtocol: class {
    var presenter: MyProfileInteractorOutputProtocol? { get set }
}

protocol MyProfileInteractorOutputProtocol: class {
    
}

protocol MyProfileViewProtocol: class {
    var presenter: MyProfilePresenterProtocol! { get set }
}
