//
//  ScanCodeViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ScanCodeViewController: BaseViewController, ScanCodeViewProtocol {
    
    private let mainView = ScanCodeView()
    var presenter: ScanCodePresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
