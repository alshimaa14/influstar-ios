//
//  UIColorExtension.swift
//  Muscli
//
//  Created by Vortex on 6/23/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var appColor: UIColor { return .rgba(91, 83, 206, 1) }
    static var twitterThemeColor: UIColor { return .rgba(3, 169, 244, 1) }
    static var facebookThemeColor: UIColor { return .rgba(59, 89, 152, 1) }
    static var linkedinThemeColor: UIColor { return .rgba(0, 119, 183, 1) }
    static var instagramThemeColor: UIColor { return .rgba(197, 54, 164, 1) }
    static var linkTextColor: UIColor { return .rgba(91, 83, 206, 1) }
    
    static func rgba(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
        return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
    
}
