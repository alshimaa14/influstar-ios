//
//  BaseViewController.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

protocol HasActivityIndicator: class {
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
}


class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}

extension BaseViewController: HasActivityIndicator {
    
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        view.showActivityIndicator(isUserInteractionEnabled: isUserInteractionEnabled)
    }
    
    func hideActivityIndicator() {
        view.hideActivityIndicator()
    }
    
}
