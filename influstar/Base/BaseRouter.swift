//
//  BaseRouter.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BaseRouter: BaseRouterProtocol {
    
    weak var viewController: UIViewController?
    
    func showAlert(with message: String) {
        viewController?.showAlert(with: message)
    }
    
}

protocol BaseRouterProtocol: class {
    func showAlert(with message: String)
}
