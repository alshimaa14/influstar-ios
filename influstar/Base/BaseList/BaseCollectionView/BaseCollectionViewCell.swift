//
//  BaseCollectionViewCell.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BaseCollectionViewCell<Model>: UICollectionViewCell, ConfigurableCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: Model) {
        fatalError("Must override configure function")
    }
}
