//
//  LoginRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum LoginDestination {
    case addProfile
    case forgetPassword
    case profile
}

class LoginRouter: BaseRouter, LoginRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = LoginViewController()
        let interactor = LoginInteractor()
        let router = LoginRouter()
        let presenter = LoginPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func navigate(to destination: LoginDestination) {
        switch destination {
        case .addProfile:
            self.viewController?.navigationController?.pushViewController(AddProfileRouter.createModule(), animated: true)
        case .forgetPassword:
            self.viewController?.navigationController?.pushViewController(ForgetPasswordRouter.createModule(), animated: true)
        case .profile:
            AppDelegate.shared.setRootViewController(
                MyProfileRouter.createModule(),
                animated: false
            )
        }
    }
    
    func dismiss() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
