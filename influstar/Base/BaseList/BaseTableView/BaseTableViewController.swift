//
//  BaseTableViewViewProtocol.swift
//  Muscli
//
//  Created by Vortex on 6/24/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

protocol BaseListViewProtocol: class {
    func reloadData()
    func endRefreshing()
}

class BaseTableViewView: UIView {
    
    let refreshControl = UIRefreshControl()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.refreshControl = refreshControl
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BaseTableViewController<M, P: BaseListPresenterProtocol, C: BaseTableViewCell<M>>: BaseViewController, BaseListViewProtocol, UITableViewDataSource, UITableViewDelegate where P.Model == M {
    
    var baseMainView: BaseTableViewView!
    var basePresenter: P!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseMainView.tableView.dataSource = self
        baseMainView.tableView.delegate = self
        baseMainView.tableView.register(C.self, forCellReuseIdentifier: C.className)
        baseMainView.refreshControl.addAction(for: .valueChanged) { [weak self] in
            guard let self = self else { return }
            self.basePresenter.pullToRefresh()
        }
    }
    
    func reloadData() {
        baseMainView.tableView.reloadData()
    }
    
    func endRefreshing() {
        UIView.animate(withDuration: 0.2, animations: {
            self.baseMainView.refreshControl.endRefreshing()
            self.baseMainView.tableView.contentOffset = .zero
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return basePresenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: C = tableView.dequeueCell(for: indexPath)
        basePresenter.configure(cell: AnyConfigurableCell(cell), at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        fatalError("Must override heightForRow")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        basePresenter.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {
            basePresenter.loadNextPage()
        }
    }
    
    func setEmptyText(text: String) {
        baseMainView.tableView.emptyMessage(message: text)
    }
}
