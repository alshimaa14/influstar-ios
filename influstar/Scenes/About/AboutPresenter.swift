//
//  AboutPresenter.swift
//  influstar
//
//  Created by ibtdi.com on 8/22/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class AboutPresenter: AboutPresenterProtocol, AboutInteractorOutputProtocol {
    
    weak var view: AboutViewProtocol?
    private let interactor: AboutInteractorInputProtocol
    private let router: AboutRouterProtocol
    
    init(view: AboutViewProtocol, interactor: AboutInteractorInputProtocol, router: AboutRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}
