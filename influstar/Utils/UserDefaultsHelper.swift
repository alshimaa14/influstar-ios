//
//  UserDefaultsHelper.swift
//  influstar
//
//  Created by ibtdi.com on 8/15/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

class UserDefaultsHelper {
    
    static private let defaults = UserDefaults.standard
    
    static var isFirstOpenApp: Bool {
        get {
            return defaults.bool(forKey: "isFirstOpenApp")
        } set {
            defaults.set(newValue, forKey: "isFirstOpenApp")
        }
    }
    
    static var isLoggedIn: Bool {
        get {
            return defaults.bool(forKey: "isLoggedIn")
        } set {
            defaults.set(newValue, forKey: "isLoggedIn")
        }
    }
    
}
