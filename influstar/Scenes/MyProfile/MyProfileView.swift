//
//  MyProfileView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class MyProfileView: BaseCollectionViewView {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        return UIView()
    }()
    
    private lazy var profilePictureImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "profile_picture"))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 46
        return imageView
    }()
    
    lazy var plusButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "ic_plus")?.withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.bold.getFont(ofSize: 15)
        label.text = "Tarek S. Abdelhamed"
        label.textAlignment = .center
        return label
    }()
    
    private lazy var bioLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.regular.getFont(ofSize: 12)
        label.attributedText = generateBioAttributedString(text: "addShortBio".localized)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    private lazy var nameTagButton: HomeButton = {
        let button = HomeButton.create()
        button.setTitle("nameTag".localized, for: .normal)
        button.setImage(UIImage(named: "ic_nametag")?.withRenderingMode(.alwaysOriginal), for: .normal)
//        button.imageView!.constraint {
//            $0.leading(24)
//            $0.centerY(0)
//        }
        return button
    }()
    
    private lazy var shortLinkButton: HomeButton = {
        let button = HomeButton.create()
        button.setTitle("shortLink".localized, for: .normal)
        button.setImage(UIImage(named: "ic_short_link")?.withRenderingMode(.alwaysOriginal), for: .normal)
//        button.imageView!.constraint {
//            $0.leading(24)
//            $0.centerY(0)
//        }
        return button
    }()
    
    var collectionViewHeightConstraint: NSLayoutConstraint!
    
    lazy var addMoreButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ic_add_more"), for: .normal)
        return button
    }()
    
    private lazy var addMoreLabel: UILabel = {
        let label = UILabel()
        label.font = AvenirNextFont.bold.getFont(ofSize: 14)
        label.text = "addMore".localized
        label.textColor = .linkTextColor
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionView.refreshControl = nil
        collectionView.isScrollEnabled = false
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(profilePictureImageView)
        containerView.addSubview(plusButton)
        containerView.addSubview(nameLabel)
        containerView.addSubview(bioLabel)
        containerView.addSubview(nameTagButton)
        containerView.addSubview(shortLinkButton)
        containerView.addSubview(collectionView)
        containerView.addSubview(addMoreButton)
        containerView.addSubview(addMoreLabel)
    }
    
    private func setupScrollViewConstraints() {
        scrollView.pinAllEdges()
    }
    
    private func setupContainerViewConstraints() {
        containerView.pinAllEdges()
        containerView.constraint {
            $0.width(multiplier: 1.0)
        }
    }
    
    private func setupProfilePictureImageViewConstraints() {
        profilePictureImageView.constraint {
            $0.top(21)
            $0.centerX(0)
            $0.width(92)
            $0.height(92)
        }
    }
    
    private func setupPlusButtonConstraints() {
        plusButton.constraint {
            $0.top(16)
            $0.height(50)
            $0.width(50)
            $0.trailing(-16, toView: profilePictureImageView)
        }
    }
    
    private func setupNameLabelConstraints() {
        nameLabel.constraint {
            $0.topToBottom(ofView: profilePictureImageView, withPadding: 16)
            $0.centerX(0)
        }
    }
    
    private func setupBioLabelConstraints() {
        bioLabel.constraint {
            $0.topToBottom(ofView: nameLabel, withPadding: 16)
            $0.leading(16)
            $0.trailing(16)
        }
    }
    
    private func setupNameTagButtonConstraints() {
        nameTagButton.constraint {
            $0.topToBottom(ofView: bioLabel, withPadding: 32)
            $0.leadingToCenterX(ofView: containerView, withPadding: 4)
        }
    }
    
    private func setupShortLinkButtonConstraints() {
        shortLinkButton.constraint {
            $0.top(0, toView: nameTagButton)
            $0.trailingToCenterX(ofView: containerView, withPadding: -4)
        }
    }
    
    private func setupCollectionViewConstraints() {
        collectionView.constraint {
            $0.topToBottom(ofView: nameTagButton, withPadding: 0)
            $0.leading(0)
            $0.trailing(0)
//            $0.bottom(0)
            collectionViewHeightConstraint = $0.height(260)
        }
    }
    
    private func setupAddMoreButtonConstraints() {
        addMoreButton.constraint {
            $0.topToBottom(ofView: collectionView, withPadding: 10)
            $0.centerX(0)
            $0.width(40)
            $0.height(40)
        }
    }
    
    private func setupAddMoreLabelConstraints() {
        addMoreLabel.constraint {
            $0.topToBottom(ofView: addMoreButton, withPadding: 	5)
            $0.centerX(0)
            $0.bottom(15)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupScrollViewConstraints()
        setupContainerViewConstraints()
        setupProfilePictureImageViewConstraints()
        setupPlusButtonConstraints()
        setupNameLabelConstraints()
        setupBioLabelConstraints()
        setupNameTagButtonConstraints()
        setupShortLinkButtonConstraints()
        setupCollectionViewConstraints()
        setupAddMoreButtonConstraints()
        setupAddMoreLabelConstraints()
    }
    
    private func generateBioAttributedString(text: String) -> NSAttributedString {
        return NSAttributedString(string: text, attributes: [
            .font: AvenirNextFont.regular.getFont(ofSize: 12),
            .foregroundColor: UIColor.appColor,
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .paragraphStyle: NSMutableParagraphStyle.centered
        ])
    
    }
}

extension NSMutableParagraphStyle {
    class var centered: NSMutableParagraphStyle {
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        return style
    }
}
