//
//  UIScrollViewExtension.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIScrollView {
    func pinAllEdges() {
        pin(edges: .safeAreaTop(0), .leading(0), .trailing(0), .safeAreaBottom(0))
    }
}
