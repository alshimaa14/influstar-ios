//
//  MyProfileRouter.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import SideMenu

class MyProfileRouter: BaseRouter, MyProfileRouterProtocol {
    
    static func createModule() -> UIViewController {
        let view = MyProfileViewController()
        let interactor = MyProfileInteractor()
        let router = MyProfileRouter()
        let presenter = MyProfilePresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        router.viewController = view
        view.presenter = presenter
        return view
    }
    
    func openSideMenu() {
        self.viewController?.present(SideMenuNavigationController(rootViewController: SideMenuRouter.createModule()), animated: true, completion: nil)
    }
    
}
