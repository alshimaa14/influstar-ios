//
//  ChooseLanguageView.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChooseLanguageView: UIView {
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "bg"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var chooseYourLanguageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = AvenirNextFont.demi.getFont(ofSize: 15)
        label.text = "chooseYourLanguage".localized
        return label
    }()
    
    lazy var arabicLanguageView: LanguageView = {
        let view = LanguageView()
        view.flagImageView.image = UIImage(named: "egypt")
        view.languageLabel.text = "arabic".localized
        return view
    }()
    
    lazy var englishLanguageView: LanguageView = {
        let view = LanguageView()
        view.flagImageView.image = UIImage(named: "unitedstates")
        view.languageLabel.text = "english".localized
        return view
    }()

    lazy var germanLanguageView: LanguageView = {
        let view = LanguageView()
        view.flagImageView.image = UIImage(named: "germany")
        view.languageLabel.text = "german".localized
        return view
    }()
    
    lazy var frenchLanguageView: LanguageView = {
        let view = LanguageView()
        view.flagImageView.image = UIImage(named: "france")
        view.languageLabel.text = "french".localized
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(backgroundImageView)
        addSubview(logoImageView)
        addSubview(chooseYourLanguageLabel)
        addSubview(arabicLanguageView)
        addSubview(englishLanguageView)
        addSubview(germanLanguageView)
        addSubview(frenchLanguageView)
    }
    
    private func setupBackgroundImageViewConstraints() {
        backgroundImageView.pinAllEdges()
    }
    
    private func setupLogoImageViewConstraints() {
        logoImageView.constraint {
            $0.safeAreaTop(64)
            $0.centerX(0)
            $0.width(171)
            $0.height(34)
        }
    }
    
    private func setupChooseYourLanguageLabelConstraints() {
        chooseYourLanguageLabel.constraint {
            $0.topToBottom(ofView: logoImageView, withPadding: 104)
            $0.centerX(0)
        }
    }
    
    private func setupArabicLanguageViewConstraints() {
        arabicLanguageView.constraint {
            $0.topToBottom(ofView: chooseYourLanguageLabel, withPadding: 35)
            $0.centerX(-75)
        }
    }
    
    private func setupEnglishLanguageViewConstraints() {
        englishLanguageView.constraint {
            $0.topToBottom(ofView: chooseYourLanguageLabel, withPadding: 35)
            $0.centerX(75)
        }
    }
    
    private func setupGermanLanguageViewConstraints() {
        germanLanguageView.constraint {
            $0.topToBottom(ofView: arabicLanguageView, withPadding: 31)
            $0.centerX(0, toView: arabicLanguageView)
        }
    }
    
    private func setupFrenchLanguageViewConstraints() {
        frenchLanguageView.constraint {
            $0.topToBottom(ofView: englishLanguageView, withPadding: 31)
            $0.centerX(0, toView: englishLanguageView)
        }
    }
    
    private func layoutUI() {
        addSubviews()
        setupBackgroundImageViewConstraints()
        setupLogoImageViewConstraints()
        setupChooseYourLanguageLabelConstraints()
        setupArabicLanguageViewConstraints()
        setupEnglishLanguageViewConstraints()
        setupGermanLanguageViewConstraints()
        setupFrenchLanguageViewConstraints()
    }
    
}
