//
//  NetworkError.swift
//  nine-hundred-trips
//
//  Created by Vortex on 4/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

enum NetworkError: Error, LocalizedError {
    case cancelled
    case noInternetConnection
    case invalidData
    case internalServerError
    case unknownError(String)
    case unauthorized
    case unverified
    case decodingFailed
    
    var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return "No Internet connection"
        case .invalidData, .internalServerError:
            return "Something went wrong"
        case .unknownError(let error):
            return error
        case .cancelled:
            return "cancelled"
        case .unauthorized:
            return "Unauthorized"
        default:
            return ""
        }
    }
}
