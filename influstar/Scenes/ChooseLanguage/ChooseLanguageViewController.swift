//
//  ChooseLanguageViewController.swift
//  influstar
//
//  Created by iOS ibtdi.com on 7/21/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class ChooseLanguageViewController: BaseViewController, ChooseLanguageViewProtocol {
    
    private let mainView = ChooseLanguageView()
    var presenter: ChooseLanguagePresenterProtocol!
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        addTargets()
    }
    
    private func addTargets() {
        let arabicGesture = UITapGestureRecognizer(target: self, action: #selector(self.arabicViewTapped(sender: )))
        mainView.arabicLanguageView.addGestureRecognizer(arabicGesture)
        
        let englishGesture = UITapGestureRecognizer(target: self, action: #selector(self.englishViewTapped(sender: )))
        mainView.englishLanguageView.addGestureRecognizer(englishGesture)
        
        let germanGesture = UITapGestureRecognizer(target: self, action: #selector(self.germanViewTapped(sender: )))
        mainView.germanLanguageView.addGestureRecognizer(germanGesture)
        
        let frenchGesture = UITapGestureRecognizer(target: self, action: #selector(self.frenchViewTapped(sender: )))
        mainView.frenchLanguageView.addGestureRecognizer(frenchGesture)
    }

    @objc func arabicViewTapped(sender: UITapGestureRecognizer) {
        presenter.navigateToProfile(language: .arabic)
    }
    
    @objc func englishViewTapped(sender: UITapGestureRecognizer) {
        presenter.navigateToProfile(language: .english)
    }
    
    @objc func germanViewTapped(sender: UITapGestureRecognizer) {
        presenter.navigateToProfile(language: .german)
    }
    
    @objc func frenchViewTapped(sender: UITapGestureRecognizer) {
        presenter.navigateToProfile(language: .french)
    }
    
}
