//
//  Constants.swift
//  Muscli
//
//  Created by Vortex on 6/25/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Regex {
        static let email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        static let password = "^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$"
        static let userName = "^[a-z0-9._-]{3,}$"
    }
    
    static let url = "http://www.influstar.dev.ibtdi.work"
    static let baseUrl = "\(url)/api/"
    
    //MARK:- AUTH ENDPOINTS
    static let addProfile = "register"
    static let login = "login"
    static let forgetPassword = "password_email"
    static let verificationCode = "password_email_code"
    static let newPassword = "password_reset"

}
